package com.coeusui;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableScheduling
public class DbBatchController {
	
	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	Job job;
	
	@Autowired
	TransformClient rClilent;
	
	@GetMapping(value="/exportToDb")
	public void uploadCSV() throws Exception {
		
	rClilent.transform();
	Thread.sleep(1000*25);
	// logic to find transform completed
	JobParameters params = new JobParametersBuilder().addString("JobID", String.valueOf(System.currentTimeMillis()))
	.toJobParameters();
	jobLauncher.run(job, params);
	}

}
