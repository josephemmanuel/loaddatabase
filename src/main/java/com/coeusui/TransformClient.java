package com.coeusui;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name="cdt", url="http://${CDT_SERVICE_HOST}:8081"/*,value="cdt"*/)
public interface TransformClient {
	@RequestMapping(method = RequestMethod.GET, value = "/transform")
	public void transform();
}
