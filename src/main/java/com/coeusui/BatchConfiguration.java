package com.coeusui;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import com.coeusui.model.Coeusui;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	public DataSource dataSource;

	@Value("file:/home/output/output*.csv")  //"file:C:/Users/support/eclipse-workspace/ExportToDatabase/src/main/resources/input/performance*.csv")
	private Resource[] inputResources;

	/*
	 * @Bean public DriverManagerDataSource dataSource() { final
	 * DriverManagerDataSource dataSource = new DriverManagerDataSource();
	 * dataSource.setDriverClassName("com.mysql.jdbc.Driver");
	 * dataSource.setUrl("jdbc:mysql://localhost/springbatch");
	 * dataSource.setUsername("root"); dataSource.setPassword("root");
	 * 
	 * return dataSource; }
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Bean
	public FlatFileItemReader<Coeusui> reader() {
		// Create reader instance
		FlatFileItemReader<Coeusui> reader = new FlatFileItemReader<Coeusui>();

		reader.setLinesToSkip(1);
		// Configure how each line will be parsed and mapped to different values
		reader.setLineMapper(new DefaultLineMapper() {
			{
				// 3 columns in each row
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "userName", "userId", "deviceName", "dateTime", "parameter", "metricvalue",  "location" });
					}
				});
				// Set values in Employee class
				setFieldSetMapper(new BeanWrapperFieldSetMapper<Coeusui>() {
					{
						setTargetType(Coeusui.class);
					}
				});
			}
		});
		return reader;
	}


	@Bean
	public CoeusuiItemProcessor processor() {
		return new CoeusuiItemProcessor();
	}

	@Bean
	public JdbcBatchItemWriter<Coeusui> writer() {
		JdbcBatchItemWriter<Coeusui> writer = new JdbcBatchItemWriter<Coeusui>();
		writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Coeusui>());
		writer.setSql("INSERT INTO coeusui (userName, userId, deviceName,  dateTime, parameter, metricvalue, location) VALUES "
				+ "(:userName, :userId, :deviceName, :dateTime, :parameter, :metricvalue,  :location)");
		writer.setDataSource(dataSource);

		return writer;
	}

	@Bean
	public Step step1() {
		return stepBuilderFactory.get("step1").<Coeusui, Coeusui>chunk(500).reader(multiResourceItemReader()).writer(writer()).build();
	}

	@Bean
	public MultiResourceItemReader<Coeusui> multiResourceItemReader() {
		MultiResourceItemReader<Coeusui> resourceItemReader = new MultiResourceItemReader<Coeusui>();
		resourceItemReader.setResources(inputResources);
		resourceItemReader.setDelegate(reader());
		return resourceItemReader;
	}

	@Bean
	public Job importUserJob() {
		return jobBuilderFactory.get("importUserJob").incrementer(new RunIdIncrementer()).start(step1()).build();
	}
}
